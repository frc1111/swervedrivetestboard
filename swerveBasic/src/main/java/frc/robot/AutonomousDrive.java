package frc.robot;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Objects;
import com.revrobotics.CANSparkMax;

public class AutonomousDrive {
    public double previousPosition0 = 0;
    public double previousPosition1 = 0;
    public double previousPosition2 = 0;
    public double previousPosition3 = 0;

    public AutonomousDrive() {

    }
    
    
    /**
    * @param desiredAngle in radians
    * @return autoRotate
    */
    public double turnToAngle (double desiredAngle, double rotSpeed) {
        double currentAngle = (Objects.navx.getAngle() * Math.PI)/180;
        double difference = closestAngleCalculator(currentAngle, desiredAngle);
        double percentError = rotSpeed*(difference / (Math.PI)); //proportion error control
        return percentError;
    }
    
    /**
     * 
     * Calculates the closest angle and direction between two points on a circle.
     * @param actualRot <ul><li>where you currently are</ul></li>
     * @param wantedRot <ul><li>where you want to end up</ul></li>
     * @return <ul><li>signed double of the angle (rad) between the two points</ul></li>
     */
    public double closestAngleCalculator(double actualRot, double wantedRot) {
        double signedDiff = 0.0;
        double rawDiff = actualRot > wantedRot ? actualRot - wantedRot : wantedRot - actualRot; // find the positive raw distance between the angles
        double modDiff = rawDiff % (2 * Math.PI); // constrain the difference to a full circle

        if (modDiff > Math.PI) { // if the angle is greater than half a rotation, go backwards
            signedDiff = ((2 * Math.PI) - modDiff); //full circle minus the angle
            if (wantedRot > actualRot) signedDiff = signedDiff * -1; // get the direction that was lost calculating raw diff
        }
        else {
            signedDiff = modDiff;
            if (actualRot > wantedRot) signedDiff = signedDiff * -1;
        }
        return signedDiff;
    }
    public double[] calculateDisplacement (CANSparkMax driveMotor, double encoderRotation) {
        double actualAngle = (Objects.navx.getYaw() * Math.PI)/180 + encoderRotation;
        double currentAngle = driveMotor.getEncoder().getPosition();
        double velocity;
        velocity =  currentAngle - previousPosition0;
        velocity = (velocity / 8.16) * 4 * Math.PI * .0254;
        double distance = velocity; 
        previousPosition0 = driveMotor.getEncoder().getPosition();
        double xDistance = Math.cos(actualAngle)*(distance);
        double yDistance = Math.sin(actualAngle)*(distance);
        double distances[] = {xDistance,yDistance};
        return distances;
    }



    public double[] translateToPosition(double desiredPositionX, double desiredPositionY, double speedScale) {
        double xDisplace = Objects.navx.getDisplacementX();
        double yDisplace = Objects.navx.getDisplacementY();
        double xMovement = desiredPositionX - xDisplace;
        double yMovement = desiredPositionY - yDisplace;
        double[] componentSpeeds = {0,0};
        if (Math.abs(yMovement)>1) {
            componentSpeeds[1] = speedScale;
        } else {
            componentSpeeds[1] = speedScale * yMovement;
        }

        if (Math.abs(xMovement)>1) {
            componentSpeeds[0] = speedScale;
        } else {
            componentSpeeds[0] = speedScale * xMovement;
        }
        
        
        return componentSpeeds;
    }

}
