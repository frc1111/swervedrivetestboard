package frc.robot;

import java.lang.Math;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;

public class LedDriver {
    int numLedsPerSide = 36;
    int totalLeds = (int)(numLedsPerSide * 4.0);
    int robotWidth = 24;
    double ledsPerInch = (double)numLedsPerSide / (double)robotWidth;

    public int calcLedAtAngle (double centerAngle) {
        double angle = centerAngle * ((Math.PI) / 180);
        
        double circleX = Math.cos(angle); // cartesian on circle
        double circleY = Math.sin(angle);

        double u = Math.max(Math.abs(circleX), Math.abs(circleY));

        double frameIntersectX = circleX / u; //one of these will be + or - 1, other will be scale to meet the circumscribed square
        double frameIntersectY = circleY / u;

        int side = 0;
        int ledNum = 0;

        // eval + and - X sides (side 0 and 2)
        if (frameIntersectX == 1) {
            side = 0;
            double normY = (frameIntersectY + 1.0) / 2.0; //move from -1 <--> 1 range to 0 <--> 1 range
            double yValInches = normY * robotWidth;
            ledNum = (int)Math.round(yValInches * ledsPerInch) + (numLedsPerSide * side);
        }
        if (frameIntersectX == -1) {
            side = 2;
            double normY = 1 - ((frameIntersectY + 1.0) / 2.0); //move from -1 <--> 1 range to 0 <--> 1 range
            double yValInches = normY * robotWidth;
            ledNum = (int)Math.round(yValInches * ledsPerInch) + (numLedsPerSide * side);
        }

        // eval + and - Y sides (side 1 and 3)
        if (frameIntersectY == 1) {
            side = 1;
            double normX = 1 - ((frameIntersectX + 1.0) / 2.0); //move from -1 <--> 1 range to 0 <--> 1 range
            double xValInches = normX * robotWidth;
            ledNum = (int)Math.round(xValInches * ledsPerInch) + (numLedsPerSide * side);
        }
        if (frameIntersectY == -1) {
            side = 3;
            double normX = (frameIntersectX + 1.0) / 2.0; //move from -1 <--> 1 range to 0 <--> 1 range
            double xValInches = normX * robotWidth;
            ledNum = (int)Math.round(xValInches * ledsPerInch) + (numLedsPerSide * side);
        }

        return ledNum;
    }

    private int[] calcLedMaxMin(double centerAngle, double angleWidth) {
        double halfAngleWidth = angleWidth / 2;

        double maxAngle = centerAngle + halfAngleWidth;
        double minAngle = centerAngle - halfAngleWidth;

        int maxLedNum = calcLedAtAngle(maxAngle);
        int minLedNum = calcLedAtAngle(minAngle);

        int[] returnArray = {maxLedNum, minLedNum};
        return returnArray;
    }

    private void populateLedRange(int maxLedNum, int minLedNum, AddressableLED m_led, AddressableLEDBuffer m_ledBuffer, int r, int g, int b) {

        if ((maxLedNum - minLedNum) < 0) { // deals with looping around from last led to first led
            for (int i = minLedNum; i <= minLedNum + (totalLeds + (maxLedNum - minLedNum)); i++) {
                m_ledBuffer.setRGB(i % m_ledBuffer.getLength(), r, g, b); //modulo constrains pixel number to within the rance of the total number of leds
            }
        }
        else { //normal case when not looping around
            for (int i = minLedNum; i <= minLedNum + (maxLedNum - minLedNum); i++) {
                m_ledBuffer.setRGB(i % m_ledBuffer.getLength(), r, g, b);
            }
        }
    }

    public void setLedRange (double centerAngle, double angleWidth, AddressableLED m_led, AddressableLEDBuffer m_ledBuffer, int r, int g, int b) {

        for (int i = 0; i < m_ledBuffer.getLength(); i++) {
            m_ledBuffer.setRGB(i, 0,0,0);
        }
        int[] ledMaxMinFront = calcLedMaxMin(centerAngle + 90, angleWidth);
        populateLedRange(ledMaxMinFront[0], ledMaxMinFront[1], m_led, m_ledBuffer, 0, 255, 0);

        int[] ledMaxMinBack = calcLedMaxMin(centerAngle - 90, angleWidth);
        populateLedRange(ledMaxMinBack[0], ledMaxMinBack[1], m_led, m_ledBuffer, 255, 0, 0);

        int[] ledMaxMinSide0 = calcLedMaxMin(centerAngle, angleWidth);
        populateLedRange(ledMaxMinSide0[0], ledMaxMinSide0[1], m_led, m_ledBuffer, r, g, b);

        int[] ledMaxMinSide1 = calcLedMaxMin(centerAngle + 180, angleWidth);
        populateLedRange(ledMaxMinSide1[0], ledMaxMinSide1[1], m_led, m_ledBuffer, r, g, b);

        m_led.setData(m_ledBuffer);
    }
}
