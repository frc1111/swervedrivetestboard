package frc.robot;
import frc.robot.SwerveDrive;
import com.kauailabs.navx.frc.AHRS; 
import edu.wpi.first.wpilibj.Relay;
import edu.wpi.first.wpilibj.Relay.Direction;

public class Objects {
    SwerveDrive swerveDrive = new SwerveDrive();
    Motors motors = new Motors();
    public static AHRS navx = new AHRS();
    public static Relay relay = new Relay(0, Direction.kReverse);
    public static Driver driver = new Driver();
    public static AutonomousDrive autonomousDrive = new AutonomousDrive ();
}