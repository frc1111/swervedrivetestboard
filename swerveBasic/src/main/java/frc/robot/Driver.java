package frc.robot;
import frc.robot.Objects;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.AddressableLED;
import edu.wpi.first.wpilibj.AddressableLEDBuffer;
import edu.wpi.first.wpilibj.Relay;


public class Driver {
    private final XboxController m_controller = new XboxController(0);
    private final Joystick m_JoystickLeft = new Joystick(0);
    private final Joystick m_JoystickRight = new Joystick(1);
    public double xDisplacement = 0;
    public double yDisplacement = 0;
   
    public void drive(Objects object) {
    // double translateX = m_controller.getX(GenericHID.Hand.kLeft);
        // double translateY = m_controller.getY(GenericHID.Hand.kLeft);
        // double rotate =     m_controller.getX(GenericHID.Hand.kRight);
        double translateX = m_JoystickLeft.getRawAxis(0);
        double translateY = m_JoystickLeft.getRawAxis(1);
        double rotate = m_JoystickRight.getRawAxis(0);
        double xDisplace = Objects.navx.getDisplacementX();
        double yDisplace = Objects.navx.getDisplacementY();
        SmartDashboard.putNumber("xDisplace", xDisplace);
        SmartDashboard.putNumber("yDisplace", yDisplace);
        
        if (m_JoystickLeft.getRawButton(1)){
            object.swerveDrive.setAutonomousRates(0,0, object.autonomousDrive.turnToAngle(0, 5));
            
            object.swerveDrive.allWheelDrive(translateX, translateY, rotate, 0.3, 1, true, false, true);
            
        } else {
            xDisplace += object.autonomousDrive.calculateDisplacement(object.motors.driveMotor0, object.swerveDrive.mod0CurrAngle)[0];
            yDisplace += object.autonomousDrive.calculateDisplacement(object.motors.driveMotor0, object.swerveDrive.mod0CurrAngle)[1];
            
            SmartDashboard.putNumber("xDisplace", xDisplace);
            SmartDashboard.putNumber("yDisplace", yDisplace);

            object.swerveDrive.allWheelDrive(translateX, translateY, rotate, 0.3, 1, true, false, false);
        
        }
    }
}
